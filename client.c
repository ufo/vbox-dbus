#include <glib.h>
#include <dbus/dbus-glib.h>
#include <stdlib.h>
#include <string.h>
#include "common-defs.h"

#include "guestprop-client-stub.h"
#include "guestprop-marshaller.h"

static void handleError(const char* msg, const char* reason,
                                               gboolean fatal) {
    g_printerr(PROGNAME ": ERROR : %s (%s)\n", msg , reason);
    if (fatal) {
        exit(EXIT_FAILURE);
    }
}

static void guestPropertyChangedSignalHandler(DBusGProxy *proxy,
                                      const char *key, const char *value,
                                      gpointer userData) {
    GError *error = NULL;
    g_print(PROGNAME ":value - changed (%s : %s)\n", key, value);
    g_clear_error(&error);
}

int main(int argc , char ** argv) {
    DBusGConnection *bus;
    DBusGProxy *remoteValue;
    GMainLoop *mainloop ;
    GError *error = NULL;
    char *property = NULL;
    
    g_type_init();
    mainloop = g_main_loop_new(NULL , FALSE);

    if (mainloop == NULL) {
        handleError("Failed to create the mainloop", "Unknown (OOM ?)",
                    TRUE);
    }
    g_print(PROGNAME ":main Connecting to Session D-Bus .\n");
    bus = dbus_g_bus_get(DBUS_BUS_SYSTEM , &error);
    if (error != NULL) {
        handleError("Couldn't connect to the Session bus", error->message,
                    TRUE);
    }
    g_print(PROGNAME ":main Creating a GLib proxy object for Value .\n");

    remoteValue =
        dbus_g_proxy_new_for_name(bus ,
                                  GUESTPROP_SERVICE_NAME,
                                  GUESTPROP_SERVICE_OBJECT_PATH,
                                  GUESTPROP_SERVICE_INTERFACE);
    if (remoteValue == NULL) {
        handleError("Couldn't create the proxy object",
                    "Unknown (dbus_g_proxy_new_for_name)", TRUE);
    }

    g_print(PROGNAME ":main Registering signal handler signatures .\n");

    dbus_g_object_register_marshaller(_guestprop_marshal_VOID__STRING_STRING,
                                      G_TYPE_NONE,
                                      G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INVALID);

    { /* Create a local scope for variables . */
        int i;
        const gchar* signalNames [] = { SIGNAL_GUEST_PROPERTY_CHANGE };

        for (i = 0; i < sizeof(signalNames) / sizeof(signalNames[0]); i++) {
            dbus_g_proxy_add_signal(remoteValue ,
                                    signalNames[i],
                                    G_TYPE_STRING,
                                    G_TYPE_STRING,
                                    G_TYPE_INVALID);
    }
}

    g_print(PROGNAME ":main Registering D-Bus signal handlers .\n");
    dbus_g_proxy_connect_signal(remoteValue,
                                SIGNAL_GUEST_PROPERTY_CHANGE,
                                G_CALLBACK (guestPropertyChangedSignalHandler),
                                NULL,
                                NULL);

    error = NULL;
    org_agorabox_vbox_GuestProperty_set_property(remoteValue, "/UFO/Test", "Test", &error);
    g_print(PROGNAME ":main set property '/UFO/Test' with value 'Test'\n");

    error = NULL;
    org_agorabox_vbox_GuestProperty_get_property(remoteValue, "/UFO/Test", &property, &error);
    g_print(PROGNAME ":main got property %s\n", property);

    g_print(PROGNAME ":main Starting main loop (first timer in 1s).\n");
    g_main_loop_run(mainloop);
    return EXIT_FAILURE ;
}

