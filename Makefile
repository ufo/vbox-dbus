PROGNAME := vbox-dbus
VERSION  := 0.1

interface_xml := guestprop-dbus-interface.xml
pkg_packages := dbus-1 dbus-glib-1 gthread-2.0
PKG_CFLAGS := $(shell pkg-config --cflags $(pkg_packages))
PKG_LDFLAGS := $(shell pkg-config --libs $(pkg_packages))
ADD_CFLAGS := -g -Wall -DG_DISABLE_DEPRECATED -DVBOX -DVBOX_OSE -DVBOX_WITH_64_BITS_GUESTS -DVBOX_WITH_HARDENING -DRT_OS_LINUX -D_FILE_OFFSET_BITS=64 -DRT_ARCH_X86 -D__X86__ -DIN_RING3 -DHC_ARCH_BITS=32 -DVBOX_WITH_GUEST_PROPS
ADD_LDFLAGS := /usr/lib/virtualbox/devel/additions/VBoxGuestR3Lib.a /usr/lib/virtualbox/devel/additions/RuntimeGuestR3Shared.a /usr/lib/virtualbox/devel/additions/VBoxGuestR3LibShared.a -lpthread
CFLAGS := $(PKG_CFLAGS) $(ADD_CFLAGS) $(CFLAGS) 
LDFLAGS := $(PKG_LDFLAGS) $(ADD_LDFLAGS) $(LDFLAGS)

cleanfiles := guestprop-client-stub.h \
              guestprop-server-stub.h
targets = server client
.PHONY: all clean checkxml
all: $(targets)

server: server.o guestprop-marshaller.o
	$(CC) $^ -o $@ $(LDFLAGS)
client: client.o guestprop-marshaller.o
	$(CC) $^ -o $@ $(LDFLAGS)

server.o: server.c common-defs.h guestprop-server-stub.h guestprop-marshaller.h
	$(CC) $(CFLAGS) -DPROGNAME=\"$(PROGNAME)\" -c $< -o $@
client.o: client.c common-defs.h guestprop-client-stub.h guestprop-marshaller.h
	$(CC) $(CFLAGS) -DPROGNAME=\"$(PROGNAME)\" -c $< -o $@

guestprop-marshaller.o: guestprop-marshaller.c guestprop-marshaller.h
	$(CC) $(CFLAGS) -c $< -o $@

guestprop-server-stub.h: $(interface_xml)
	dbus-binding-tool --prefix=guestprop --mode=glib-server \
    $< > $@

guestprop-client-stub.h: $(interface_xml)
	dbus-binding-tool --prefix=guestprop --mode=glib-client \
    $< > $@

guestprop-marshaller.h:
	glib-genmarshal --prefix _guestprop_marshal --header guestprop_marshaller.list > $@

guestprop-marshaller.c:
	glib-genmarshal --prefix _guestprop_marshal --body guestprop_marshaller.list > $@

checkxml : $(interface_xml)
	@xmllint --valid --noout $<
	@echo $< checks out ok

monitor:
	dbus-monitor --session type='signal',interface='org.agorabox.vbox.GuestProperty',member='changed' | while read line; \
	do \
		if [ "$$line" != "$${line%%member='changed'}" ]; \
		then \
			read key; \
			read value; \
			key=$$(echo $$key | tr -d '\"'); \
			value=$$(echo $$value | tr -d '\"'); \
			echo "Got guest property $$key with value $$value"; \
		fi; \
	done

clean:
	$(RM) $(targets) $(cleanfiles) guestprop-marshaller.h guestprop-marshaller.c *.o

server.o client.o: Makefile

install:
	install -m 755 -D server $(DESTDIR)/usr/libexec/$(PROGNAME)
	install -m 644 -D guestprop-dbus-interface.xml $(DESTDIR)/usr/share/dbus-1/interfaces/guestprop-dbus-interface.xml
	install -m 644 -D service/org.agorabox.vbox.conf $(DESTDIR)/etc/dbus-1/system.d/org.agorabox.vbox.conf
	install -m 644 -D service/org.agorabox.vbox.service $(DESTDIR)/usr/share/dbus-1/system-services/org.agorabox.vbox.service
	install -m 755 -D init/vbox-dbus $(DESTDIR)/etc/rc.d/init.d/vbox-dbus

archive:
	git archive --format tar --prefix $(PROGNAME)-$(VERSION)/ HEAD | bzip2 > $(PROGNAME)-$(VERSION).tar.bz2
