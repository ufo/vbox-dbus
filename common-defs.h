#ifndef INCLUDE_COMMON_DEFS_H
# define INCLUDE_COMMON_DEFS_H

# define GUESTPROP_SERVICE_NAME             "org.agorabox.vbox"
# define GUESTPROP_SERVICE_OBJECT_PATH      "/GuestProperty"
# define GUESTPROP_SERVICE_INTERFACE        "org.agorabox.vbox.GuestProperty"

# define SIGNAL_GUEST_PROPERTY_CHANGE       "changed"
#endif

