#include <glib.h>
#include <dbus/dbus-glib.h>
#include <stdlib.h>
#include <unistd.h>

#include "common-defs.h"

#undef bool
#include <VBox/err.h>
#include <VBox/VBoxGuest.h>
#include <VBox/VBoxGuestLib.h>
#include <iprt/initterm.h>

#include <iprt/thread.h>
#include <iprt/alloc.h>
#include <iprt/string.h>

#include "guestprop-marshaller.h"

#define SUCCESS_STATUS_GP_VALUE   "OK"
#define FAILURE_STATUS_GP_VALUE   "FAILED"
#define NOPASS_GP_STATUS_VALUE    "NO_PASSWORD"

enum { MAX_NAME_LEN = 64 };
enum { MAX_VALUE_LEN = 128 };
enum { MAX_FLAGS_LEN =   sizeof("TRANSIENT, RDONLYGUEST") };

typedef enum {
    GUEST_PROPERTY_CHANGED_SIGNAL,
    GUEST_PROPERTY_SIGNAL_COUNT
} GuestPropertySignalNumber;

typedef struct {
    GObject parent;
    gchar *key;
    gchar *value;
} GuestProperty;

typedef struct {
    GObjectClass parent;
    guint signals [ GUEST_PROPERTY_SIGNAL_COUNT ];
} GuestPropertyClass;

GType guestprop_get_type(void);
gboolean guestprop_set_property(GuestProperty *obj, const gchar *key, const gchar *value);
gboolean guestprop_get_property(GuestProperty *obj, const gchar *key, gchar **value);

#define GUESTPROP_TYPE_OBJECT                 ( guestprop_get_type ())
#define GUESTPROP_OBJECT (object) \
        (G_TYPE_CHECK_INSTANCE_CAST ((object), \
         GUESTPROP_TYPE_OBJECT, GuestProperty))
#define GUESTPROP_OBJECT_CLASS(klass) \
        (G_TYPE_CHECK_CLASS_CAST ((klass), \
         GUESTPROP_TYPE_OBJECT, GuestPropertyClass))
#define GUESTPROP_IS_OBJECT(object) \
        (G_TYPE_CHECK_INSTANCE_TYPE((object), \
         GUESTPROP_TYPE_OBJECT))
#define GUESTPROP_IS_OBJECT_CLASS(klass) \
        (G_TYPE_CHECK_CLASS_TYPE((klass), \
         GUESTPROP_TYPE_OBJECT))
#define GUESTPROP_OBJECT_GET_CLASS(obj) \
        (G_TYPE_INSTANCE_GET_CLASS((obj), \
         GUESTPROP_TYPE_OBJECT, GuestPropertyClass))
G_DEFINE_TYPE(GuestProperty, guestprop, G_TYPE_OBJECT)

#include "guestprop-server-stub.h"

#ifdef NO_DAEMON
# define dbg(fmtstr , args ...) \
   ( g_print ( PROGNAME ":%s: " fmtstr "\n", __func__ , ## args))
#else
# define dbg(dummy ...)
#endif

/* Global variables */
uint32_t u32ClientId;
uint64_t *timestamp;
DBusGConnection *bus = NULL;

static void handleError(const char* msg, const char* reason,
                        gboolean fatal) {
    g_printerr(PROGNAME ": ERROR : %s (%s)\n", msg, reason);
    if (fatal) {
       exit(EXIT_FAILURE);
    }
}

static void guestprop_init(GuestProperty *obj) {
    dbg("Called");
    g_assert(obj != NULL);
}

static void guestprop_class_init(GuestPropertyClass *klass) {
    const gchar* signalNames [GUEST_PROPERTY_SIGNAL_COUNT] = {
        SIGNAL_GUEST_PROPERTY_CHANGE
    };
    guint signalId;
    signalId =
       g_signal_new(signalNames[0],
                    G_OBJECT_CLASS_TYPE(klass),
                    G_SIGNAL_RUN_LAST,
                    0,
                    NULL,
                    NULL,
                    _guestprop_marshal_VOID__STRING_STRING,
                    G_TYPE_NONE,
                    2,
                    G_TYPE_STRING,
                    G_TYPE_STRING);
    klass->signals[0] = signalId;
    dbus_g_object_type_install_info(GUESTPROP_TYPE_OBJECT,
                                    &dbus_glib_guestprop_object_info);
    dbg("Done");
}

static void guestprop_emitSignal(GuestProperty *obj,
                                 GuestPropertySignalNumber num,
                                 const gchar *key, const gchar *value)
{

    GuestPropertyClass *klass = GUESTPROP_OBJECT_GET_CLASS(obj);
    g_assert((num < GUEST_PROPERTY_SIGNAL_COUNT) && (num >= 0));
    dbg("Emitting signal id %d, with message %s : %s", num, key, value);
    g_signal_emit(obj,
                  klass->signals[num],
                  0,
                  key,
                  value);
}

gboolean guestprop_set_property(GuestProperty *obj, const gchar *key, const gchar *value)
{
    // GuestPropertyClass *klass = GUESTPROP_OBJECT_GET_CLASS(obj);
    dbg("Setting guest property %s : %s", key, value);
    VbglR3GuestPropWriteValue(u32ClientId, key, value);
    return false;
}

gboolean guestprop_get_property(GuestProperty *obj, const char *key, char **value)
{
    unsigned int i;
    char *pszValue = NULL;
    uint64_t u64Timestamp = 0;
    char *pszFlags = NULL;
    void *pvBuf = NULL;
    uint32_t cbBuf = MAX_VALUE_LEN + MAX_FLAGS_LEN + 1024;
    void *pvTmpBuf;
    int rc;

    dbg("Retrieving guest property %s", key);
    bool finish = false;
    for (i = 0; (i < 10) && !finish; ++i)
    {
        pvTmpBuf = RTMemRealloc(pvBuf, cbBuf);
        if (NULL == pvTmpBuf)
        {
            rc = VERR_NO_MEMORY;
        }
        else
        {
            pvBuf = pvTmpBuf;
            rc = VbglR3GuestPropRead(u32ClientId, key, pvBuf, cbBuf,
                                     &pszValue, &u64Timestamp, &pszFlags,
                                     &cbBuf);
        }
        if (VERR_BUFFER_OVERFLOW == rc)
            /* Leave a bit of extra space to be safe */
            cbBuf += 1024;
        else
            finish = true;
    }
    if(RT_SUCCESS(rc)) {
        dbg("Got %s %s", key, pszValue);
        *value = g_strdup(pszValue);
        RTMemFree(pvBuf);
        return TRUE;
    }
    return FALSE;    
}

int connect_to_service()
{
    int rc = VINF_SUCCESS;

    if (!RT_SUCCESS(RTR3Init()) || !RT_SUCCESS(VbglR3Init()))
    {
        handleError("Could not connect initialize HGCM", "Unknown", TRUE);
        return -1;
    }

    rc = VbglR3GuestPropConnect(&u32ClientId);
    if (!RT_SUCCESS(rc))
    {
        handleError("Could not connect to the guest property service", "Unknown", TRUE);
        return -1;
    }
    
    return 0;
}

gpointer guest_property_listener(gpointer data)
{
    int rc = VINF_SUCCESS;
    GuestProperty *guestprop = NULL;
    char *pattern = NULL, *value = NULL, *name = NULL;
    char *flags = NULL;
    void *buf = NULL;
    uint32_t timeout = RT_INDEFINITE_WAIT;
    uint64_t timestamp_out = 0;
    uint32_t buflen = MAX_NAME_LEN + MAX_VALUE_LEN + MAX_FLAGS_LEN + 1024;
    void *tmpbuf;
    unsigned char finish = 0;
    int i;

    RTStrCurrentCPToUtf8(&pattern, "*");

    guestprop = g_object_new(GUESTPROP_TYPE_OBJECT , NULL);
    if(guestprop == NULL) {
         handleError("Failed to create one Value instance.",
                     "Unknown (OOM ?)", TRUE);
    }
    dbus_g_connection_register_g_object(bus,
                                        GUESTPROP_SERVICE_OBJECT_PATH,
                                        G_OBJECT(guestprop));

    while(true) {
        finish = 0;
        for(i = 0;
            (RT_SUCCESS(rc) || rc == VERR_BUFFER_OVERFLOW) && !finish && (i < 10);
            ++i) {
            timestamp_out = 0;
            if (! (tmpbuf = RTMemRealloc(buf, buflen))) {
                 g_print("Out of memory\n");
                 return (gpointer) VERR_NO_MEMORY;
            }
            buf = tmpbuf;
            rc = VbglR3GuestPropWait(u32ClientId, pattern, buf, buflen,
                                     timestamp_out, timeout,
                                     &name, &value, &timestamp_out,
                                     &flags, &buflen);
        
            if (VERR_BUFFER_OVERFLOW == rc)
                /* Leave a bit of extra space to be safe */
                buflen += 1024;
            else
                finish = 1;

            if(RT_SUCCESS(rc)) {
                guestprop_emitSignal(guestprop, GUEST_PROPERTY_CHANGED_SIGNAL, name, value);
            }
            // RTMemFree(buf);
        }
    }
    return NULL;
}

int main(int argc , char ** argv) {
     DBusGProxy *busProxy = NULL;
     GMainLoop *mainloop = NULL;
     guint result;
     GError *error = NULL;

     connect_to_service();

     g_type_init();
     g_thread_init(NULL);

     mainloop = g_main_loop_new(NULL , FALSE);
     if (mainloop == NULL) {
         handleError("Couldn't create GMainLoop ", " Unknown (OOM ?)", TRUE);
     }
     bus = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);
     if (error != NULL) {
         handleError("Couldn't connect to session bus", error->message, TRUE);
     }

     busProxy = dbus_g_proxy_new_for_name(bus,
                                          DBUS_SERVICE_DBUS,
                                          DBUS_PATH_DBUS,
                                          DBUS_INTERFACE_DBUS);
     if (busProxy == NULL) {
          handleError("Failed to get a proxy for D-Bus",
                      "Unknown (dbus_g_proxy_new_for_name)", TRUE);
     }

     if (!dbus_g_proxy_call(busProxy,
                            "RequestName",
                            &error,
                            G_TYPE_STRING,
                            GUESTPROP_SERVICE_NAME,
                            G_TYPE_UINT,
                            0,
                            G_TYPE_INVALID,
                            G_TYPE_UINT,
                            &result ,
                            G_TYPE_INVALID)) {
        handleError("D-Bus. RequestName RPC failed ", error->message,
                                                      TRUE);
    }

    if(result != 1) {
        handleError("Failed to get the primary well-known name.",
                    "RequestName result != 1", TRUE);
    }

    #ifndef NO_DAEMON
    if (daemon (0, 0) != 0) {
        g_error(PROGNAME ":Failed to daemonize.\n");
    }
    #else
        g_print(PROGNAME
                ": Not daemonizing (built with NO_DAEMON -build define)\n");
    #endif

    g_thread_create(guest_property_listener, NULL, false, &error);
    g_main_loop_run(mainloop);
    return EXIT_FAILURE ;
}

