Name:           vbox-dbus
Version:        0.1
Release:        3%{?dist}
Summary:        D-Bus service for the VirtualBox additions

Group:          System/Utilies
License:        GPLv2
URL:            http://www.agorabox.org
Source0:        vbox-dbus-0.1.tar.bz2
BuildArch:      i686
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  dbus-devel VirtualBox-OSE-guest-devel dbus-glib-devel glib2-devel
Requires:       dbus VirtualBox-OSE-guest glib2 dbus-glib
Requires(postun): initscripts
Requires(post):   chkconfig
Requires(preun):  chkconfig
Requires(preun):  initscripts


%description
vbox-dbus provides a D-Bus service for accessing VirtualBox services
provided by the VirtualBox additions like Guest Properties.


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%post
/sbin/chkconfig --add vbox-dbus


%preun
if [ $1 = 0 ] ; then
    /sbin/service vbox-dbus stop >/dev/null 2>&1
    /sbin/chkconfig --del vbox-dbus
fi


%postun
if [ "$1" -ge "1" ] ; then
    /sbin/service vbox-dbus restart >/dev/null 2>&1 || :
fi


%files
%defattr(-,root,root,-)
%doc
%{_sysconfdir}/rc.d/init.d/vbox-dbus
%{_sysconfdir}/dbus-1/system.d/org.agorabox.vbox.conf
%{_datadir}/dbus-1/system-services/org.agorabox.vbox.service
%{_datadir}/dbus-1/interfaces/guestprop-dbus-interface.xml
%{_libexecdir}/vbox-dbus


%changelog
* Sat Jun 05 2010 Sylvain Baubeau <sylvain.baubeau@agorabox.org> 0.1-1
- Initial release

